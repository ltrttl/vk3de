#pragma once
#include <string>
#include <vector>
#include "Vk3De_device.hpp"
namespace Vk3De
{
	struct PipelineConfigInfo 
	{
		// PipelineConfigInfo() = default;
		// PipelineConfigInfo(const PipelineConfigInfo&) = delete;
		// PipelineConfigInfo& operator=(const PipelineConfigInfo&) = delete;
		VkViewport viewport;
		VkRect2D scissor;
		VkPipelineViewportStateCreateInfo viewportInfo;
		VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo;
		VkPipelineRasterizationStateCreateInfo rasterizationInfo;
		VkPipelineMultisampleStateCreateInfo multisampleInfo;
		VkPipelineColorBlendAttachmentState colorBlendAttachment;
		VkPipelineColorBlendStateCreateInfo colorBlendInfo;
		VkPipelineDepthStencilStateCreateInfo depthStencilInfo;
		//VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo;
		VkPipelineLayout pipelineLayout = nullptr;
		VkRenderPass renderPass = nullptr;
		uint32_t subpass = 0;
	};
	class Vk3DePipeline
	{
	private:
	void createGraphicsPipeline(
			const std::string& vertFilePath, 
			const std::string& fragFilePath, 
			const PipelineConfigInfo& configInfo);

	Vk3DeDevice& appDevice;
	VkPipeline graphicsPipeline;
	VkShaderModule vertShaderModule;
	VkShaderModule fragShaderModule;

	void createShaderModule(
		const std::vector<char>& code,
		VkShaderModule* shaderModule);

	public:
		Vk3DePipeline(
			Vk3De::Vk3DeDevice &device, //otval prostranstva imen
			const std::string& vertFilePath,
			const std::string& fragFilePath, 
			const PipelineConfigInfo& configInfo);
		static std::vector<char> readFile(const std::string& filepath);
		~Vk3DePipeline();
		Vk3DePipeline(const Vk3DePipeline&) = delete;
		void operator = (const Vk3DePipeline&) = delete;

		static PipelineConfigInfo defaultPipelineConfigInfo(uint32_t width, uint32_t height);

		void bind(VkCommandBuffer commandBuffer); //���������� � ������� ������
	};
}