#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <string>
#include <iostream>
namespace Vk3De
{
	class Vk3DeWindow
	{
	private:
		void initWindow();
		const int width;
		const int height;
		std::string windowName;
		GLFWwindow* window;
		
	public:
		Vk3DeWindow(int w, int h, std::string name);
		~Vk3DeWindow();
		bool closeEvent() { return glfwWindowShouldClose(window); };
		GLFWwindow* getWindow() { return window; } //������� GLFW ����
		Vk3DeWindow(const Vk3DeWindow&) = delete;
		Vk3DeWindow& operator = (const Vk3DeWindow) = delete;
		void createWindowSurface(VkInstance instance, VkSurfaceKHR* surface);
		VkExtent2D getExtent()
		{ 
			return {static_cast<uint32_t>(width), static_cast<uint32_t>(height)}; 
		}
	};
}