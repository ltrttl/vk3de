#include "application.hpp"
#include <cstdlib>

#include <stdexcept>

int main()
{
	Vk3De::application app;
	try
	{
		app.run();
		
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}