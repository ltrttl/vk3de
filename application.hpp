#pragma once

#include "Vk3De_window.hpp"
#include <iostream>
#include "Vk3De_pipeline.hpp"
#include "Vk3De_device.hpp"
#include "Vk3De_swap_chain.hpp"
#include <memory>
#include <vector>

namespace Vk3De
{
class application
{
private:
	Vk3DeWindow appWindow{ WIDTH, HEIGHT, "VulkanTest" };
	Vk3DeDevice appDevice{ appWindow };
	Vk3DeSwapChain appSwapChain{appDevice, appWindow.getExtent()};
	std::unique_ptr<Vk3DePipeline> appPipeline;
	VkPipelineLayout pipelineLayout;
	std::vector<VkCommandBuffer> commandBuffers;
	
	void createPipelineLayout();
	void createPipeline();
	void createCommandBuffers();
	void drawFrame();

public:
	static constexpr int WIDTH = 1600;
	static constexpr int HEIGHT = 1200;

	application();
	~application();

	application(const application&) = delete;
	application &operator = (const application&) = delete;
	void run();
};
}